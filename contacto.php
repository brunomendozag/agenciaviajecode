<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contacto</title>
    <?php 
    include('componentes/estilos.html')
    ?>
</head>
<body>
    <!--Menu-->
    <?php
    include('componentes/menu.html')
    ?>

<!--Banner de empresa-->
<div class="col-md-12 banner-contacto">
   <!--Formulario de contacto-->
   <div class="col-md-12 formulario">
        <p class="titulo">Contacto</p>
        <form action="" class="form-contactacto">
            <div class="row">
                <div class="col-md-12">
                    <input type="text" placeholder="Nombre y Apellido">
                </div>

                <div class="col-md-6">
                    <input type="text" placeholder=Correo">
                </div>

                <div class="col-md-6">
                    <input type="text" placeholder="Teléfono">
                </div>

                <div class="col-md-6">
                    <select name="" id="">
                        <option disabled selected>Seleccionar Ciudad</option>
                        <option>Asunción</option>
                        <option>San Lorenzo</option>
                        <option>Villa Elisa</option>
                    </select>
                </div>

                <div class="col-md-6">
                    <input type="text" placeholder="Codigo Postal">
                </div>

                <div class="col-md-12">
                    <input type="submit" value="Enviar" class="btn-enviar">
                </div>
            </div>
        </form>
    </div>
</div>
<div class="col-md-6 mapa">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3607.3696789661053!2d-57.63588698549563!3d-25.291780833640235!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x945da7c78c421bf7%3A0xef6b8dfbe7fadb76!2sCodelife!5e0!3m2!1ses-419!2spy!4v1578163769517!5m2!1ses-419!2spy"  height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>

<!--Formulario de contacto-->
<div class="col-md-12">
    <p class="title"></p>
</div>







    <!--Pie de pagina-->
<?php
include('componentes/footer.html')
?>

<!--Archivos Javscript-->
<?php
include('componentes/js.html')
?>

<!--Funcion del menu responsive-->
<script>
    $('.menu-responsive').hide();
    $('.lista-menu-responsive').hide();

    $('.boton-menu').click(function() {
        $('.lista-menu-responsive').fadeIn();
    })

</script>
</body>
</html>