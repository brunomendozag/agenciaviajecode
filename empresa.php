<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Empresa</title>

    <?php 
    include('componentes/estilos.html')
    ?>

</head>
<body>



    <!--Menu-->
    <?php
    include('componentes/menu.html')
    ?>

    <!--Banner de empresa-->
<div class="col-md-12 banner-empresa">
    <h1>Sobre Nosotros</h1>
</div>

<!--Contenido-->
<div class="col-md-12 contenido-empresa text-center">
    <h2>Porque elegirnos</h2>

    <div class="container">
        <div class="row">
            <div class="col-md-3 item text-center">
                <i class="fas fa-check-square"></i>
                <p>Responsabilidad</p>
            </div>

            <div class="col-md-3 item text-center">
                <i class="fas fa-dollar-sign"></i>
                <p>Precios Accesibles</p>
            </div>

            <div class="col-md-3 item text-center">
                <i class="fas fa-users"></i>
                <p>Viajes grupales</p>
            </div>
        </div>
    </div>
</div>


<!--Contenido-->
    <div class="row">
        <div class="col-md-6 imagen-uno fondo">

        </div>

        <div class="col-md-6 text-center texto-contenido">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
        </div>

        <div class="col-md-6 text-center texto-contenido">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
        </div>

        <div class="col-md-6 imagen-dos fondo">

        </div>
    </div>


    <!--Pie de pagina-->
    <?php
    include('componentes/footer.html')
    ?>

    <!--Archivos Javscript-->
    <?php
    include('componentes/js.html')
    ?>

    <!--Funcion del menu responsive-->
<script>
    $('.menu-responsive').hide();
    $('.lista-menu-responsive').hide();

    $('.boton-menu').click(function() {
        $('.lista-menu-responsive').fadeIn();
    })

</script>


</body>
</html>