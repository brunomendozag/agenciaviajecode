<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Viajes</title>
    <?php 
    include('componentes/estilos.html')
    ?>
</head>
<body>

 <!--Menu-->
    <?php
    include('componentes/menu.html')
    ?>


<!--Contenido de viajes-->
<div class="col-md-12 banner-interno">
    <h2>Viaje</h2>  
</div>


<div class="col-md-12 contenido-viaje">
    <div class="container">
        <div class="row">

            <div class="col-md-3 text-center">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3607.3696789661053!2d-57.63588698549563!3d-25.291780833640235!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x945da7c78c421bf7%3A0xef6b8dfbe7fadb76!2sCodelife!5e0!3m2!1ses-419!2spy!4v1578163769517!5m2!1ses-419!2spy"  height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                
                <h5>Acerca del viaje</h5>
                <div class="cuerpo-planes">
                        <ul>
                            <li><img src="img/check.png"> 3 viajes por mes</li>
                            <li><img src="img/check.png"> Seguro al viajero</li>
                            <li><img src="img/check.png"> Hasta 2 personas</li>
                        </ul>
                    </div>
            </div>

            <div class="col-md-6">
                <img src="img/hotel.jpg" class="img-viaje">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            </div>

            <div class="col-md-3 text-center">
                <h5>Reservar viaje</h5>
                <form action="#" class="formulario-reserva">
                    <div class="row">
                        <input type="text" placeholder="Nombre y Apellido">
                        <input type="email" placeholder="Correo">
                        <input type="text" placeholder="Telefono">
                        <input type="text" placeholder="Dirección">
                        <input type="submit" value="Reservar" class="btn btn-success" >
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>



   

<!--Pie de pagina-->
<?php
include('componentes/footer.html')
?>

<!--Archivos Javscript-->
<?php
include('componentes/js.html')
?>
</body>
</html>

