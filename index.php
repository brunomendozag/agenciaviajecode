<?php
if (isset($_POST["submit"])) {
    $nombre = $_POST['nombre'];
    $telefono = $_POST['telefono'];
    $direccion = $_POST['telefono'];
    $ciudad = $_POST['ciudad'];
    $codigo_postal = $_POST['codigo'];
    $asunto = 'Correo desde la web';
    $from = 'pereiraharles@gmail.com';
    $to = 'pereiraharles@gmail.com';
    $subject = $asunto;

    $body = "De: $nombre\n Correo: $correo\n Telefono: $telefono\n
    Direccion: $direccion\n Ciudad: $ciudad\n Codigo Postal: $codigo_postal\n";

    if (!$_POST["nombre"]) {
        $errNombre = 'Introduzca su nombre y apellido';
    }
    
// Si no hay errores, se envia el email
if(!$errNombre) {
    if (mail ($to, $subject, $body, $from)){
        $result='<div class="alert alert-success">Su mensaje ha sido enviado!.
        Se pondrán en contacto con usted en la brevedad posible.</div>';
    } else {
        $result='<div class="alert alert-danger">Hubo un error.
        Intente de nuevo mas tarde</div>';
    }
}
}

?>

<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Agencia de viajes</title>

    <?php 
    include('componentes/estilos.html')
    ?>

</head>
<body>

    <!--Boton flotante-->
    <a href="" class="boton-flotante">
        <img src="img/wha.png">
    </a>

    


    <!--Menu-->
    <?php
    include('componentes/menu.html')
    ?>

    <!--banner-->
    <div id="slide-banner" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner banner">
            <div class="carousel-item active">
                <!--Formulario del banner-->
                <h1>Realizar una búsqueda</h1>
                <form action="#" class="formulario-banner">
                    <div class="row">
                        <div class="col-md-3">
                            <input type="text" placeholder="Destino">
                        </div>

                        <div class="col-md-3">
                            <input type="date">
                        </div>

                        <div class="col-md-3">
                            <input type="text" placeholder="Dias">
                        </div>

                        <div class="col-md-3">
                            <input type="submit" value="Buscar" class="btn btn-warning">
                        </div>

                        <div class="col-md-12 text-center">
                            <a href="#" class="activar-formulario">Realizar búsqueda avanzada</a>
                        </div>

                    </div>
                </form>

                <!--Formulario avanzado del banner-->
                <form action="#" class="formulario-avanzado">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="destino">Destino</label>
                            <input type="text">
                        </div>

                        <div class="col-md-3">
                            <label for="check-in">Check-in</label>
                            <input type="date">
                        </div>

                        <div class="col-md-3">
                            <label for="check-out">Check-out</label>
                            <input type="date" name="" id="">
                        </div>

                        <div class="col-md-3">
                            <label for="habitaciones">Tipo de habitación</label>
                            <select>
                                <option value="1">Individual</option>
                                <option value="2">Doble</option>
                                <option value="3">Familiar</option>
                                <option value="4">Suite</option>
                            </select>
                        </div>

                        <div class="col-md-3">
                            <label for="adultos">Adultos</label>
                            <select>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>

                        <div class="col-md-3">
                            <label for="ninos">Ninos</label>
                            <select>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>

                        <div class="col-md-3">
                            <label for="tipo-viaje">Tipo de viaje</label>
                            <select>
                                <option value="1">En avión</option>
                                <option value="2">En bus</option>
                                <option value="3">En un crucero</option>
                            </select>
                        </div>

                        <div class="col-md-3">
                            <button class="btn btn-info btn-avanzado">Buscar</button>
                        </div>

                    </div>
                </form>

            </div>
        </div>
    </div>

    <!--Contenido-->
    <div class="col-md-12 viajes" >
        <p class="titulo">Viajes</p>
        <div class="container">
            <div class="row">
                <div class="col-md-4" data-aos="fade-right" data-aos-duration="3000">
                    <div class="cabecera-viaje">
                        <img src="img/paris.jpg">
                        <p class="precio">500$</p>
                    </div>

                    <div class="cuerpo-viaje">
                        <h3>Viaje a Paris</h3>
                        <ul>
                            <li><img src="img/check.png"> 6 noches</li>
                            <li><img src="img/check.png"> Seguro al viajero</li>
                            <li><img src="img/check.png"> Comodidad</li>
                        </ul>
                    </div>

                    <div class="reserva-viaje">
                        <button class="btn btn-success boton-reserva" data-toggle="modal" data-target="#modal-viajes">Reservar</button>
                    </div>

                </div>

                <div class="col-md-4" data-aos="fade-right" data-aos-duration="3000">
                    <div class="cabecera-viaje">
                        <img src="img/bsas.jpg">
                        <p class="precio">500$</p>
                    </div>

                    <div class="cuerpo-viaje">
                        <h3>Viaje a Buenos Aires</h3>
                        <ul>
                            <li><img src="img/check.png"> 6 noches</li>
                            <li><img src="img/check.png"> Seguro al viajero</li>
                            <li><img src="img/check.png"> Comodidad</li>
                        </ul>
                    </div>

                    <div class="reserva-viaje">
                        <button class="btn btn-success boton-reserva" data-toggle="modal" data-target="#modal-viajes">Reservar</button>
                    </div>
                </div>

                <div class="col-md-4" data-aos="fade-right" data-aos-duration="3000">
                    <div class="cabecera-viaje">
                        <img src="img/london.jpg">
                        <p class="precio">500$</p>
                    </div>

                    <div class="cuerpo-viaje">
                        <h3>Viaje a Londres</h3>
                        <ul>
                            <li><img src="img/check.png"> 6 noches</li>
                            <li><img src="img/check.png"> Seguro al viajero</li>
                            <li><img src="img/check.png"> Comodidad</li>
                        </ul>
                    </div>

                    <div class="reserva-viaje">
                        <button class="btn btn-success boton-reserva" data-toggle="modal" data-target="#modal-viajes">Reservar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--Modal-->
    <div class="modal fade" id="modal-viajes" tabindex="-1" role="dialog" aria-labelledby="modal-viajes" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h3>Modal</h3>
                </div>
            </div>
        </div>
    </div>

    <!--Contenido-->
    <div class="row" data-aos="fade-left" data-aos-duration="3000">
        <div class="col-md-4 tipo-viajes text-center">
            <div>
                <h4>Viajes en familia</h4>
                <img src="img/familia.png">
                <p>Disfrute en familia</p>
            </div>
        </div>

        <div class="col-md-4 tipo-viajes text-center">
            <div>
            <h4>Viajes de aventura</h4>
            <img src="img/aventura.png">
            <p>Disfrute increibles aventuras</p>
            </div>
        </div>

        <div class="col-md-4 tipo-viajes text-center">
            <div>
            <h4>Viajes culturales</h4>
            <img src="img/cultura.png">
            <p>Visite museos.</p>
            </div>
        </div>
    </div>

    <!--Seccion de planes-->
    <div class="col-md-12 planes" data-aos="fade-down" data-aos-duration="3000">
        <p class="titulo">Planes</p>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="cabecera-planes">
                        <p class="precio-planes">500 mil x mes</p>
                    </div>

                    <div class="cuerpo-planes">
                        <ul>
                            <li><img src="img/check.png"> 3 viajes por mes</li>
                            <li><img src="img/check.png"> Seguro al viajero</li>
                            <li><img src="img/check.png"> Hasta 2 personas</li>
                        </ul>
                    </div>

                    <div class="reserva-planes">
                        <button class="btn btn-success">Comprar Plan</button>
                    </div>

                </div>

                <div class="col-md-4">
                    <div class="cabecera-planes">
                        <p class="precio-planes">500 mil x mes</p>
                    </div>

                    <div class="cuerpo-planes">
                        <ul>
                            <li><img src="img/check.png"> 3 viajes por mes</li>
                            <li><img src="img/check.png"> Seguro al viajero</li>
                            <li><img src="img/check.png"> Hasta 2 personas</li>
                        </ul>
                    </div>

                    <div class="reserva-planes">
                        <button class="btn btn-success">Comprar Plan</button>
                    </div>

                </div>

                <div class="col-md-4">
                    <div class="cabecera-planes">
                        <p class="precio-planes">500 mil x mes</p>
                    </div>

                    <div class="cuerpo-planes">
                        <ul>
                            <li><img src="img/check.png"> 3 viajes por mes</li>
                            <li><img src="img/check.png"> Seguro al viajero</li>
                            <li><img src="img/check.png"> Hasta 2 personas</li>
                        </ul>
                    </div>

                    <div class="reserva-planes">
                        <button class="btn btn-success">Comprar Plan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Seccion de Testimonios-->
    <div class="col-md-12 testimonios">
        <p class="titulo">Testimonios</p>

        <div class="container">

        <!--Carousel-->
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">

    <div class="carousel-item active">
      <div class="row">
          <div class="col-md-6 text-center">
            <div class="card-testimonio">
                <img src="img/testimonio.png">
                <p>"Muy buenos viajes, recomendado."</p>
                <ul>
                    <li><img src="img/star.png"></li>
                    <li><img src="img/star.png"></li>
                    <li><img src="img/star.png"></li>
                    <li><img src="img/star.png"></li>
                </ul>
                <h5>Harles Pereira</h5>
            </div>
          </div>

          <div class="col-md-6 text-center">
          <div class="card-testimonio">
                <img src="img/testimonio2.png">
                <p>"Muy buenos viajes, recomendado."</p>
                <ul>
                    <li><img src="img/star.png"></li>
                    <li><img src="img/star.png"></li>
                    <li><img src="img/star.png"></li>
                    <li><img src="img/star.png"></li>
                    <li><img src="img/star.png"></li>
                </ul>
                <h5>Juan Perez</h5>
            </div>
          </div>
      </div>
    </div>

    <div class="carousel-item">
    <div class="row">
          <div class="col-md-6 text-center">
            <div class="card-testimonio">
                <img src="img/testimonio.png">
                <p>"Muy buenos viajes, recomendado."</p>
                <ul>
                    <li><img src="img/star.png"></li>
                    <li><img src="img/star.png"></li>
                    <li><img src="img/star.png"></li>
                    <li><img src="img/star.png"></li>
                </ul>
                <h5>Harles Pereira</h5>
            </div>
          </div>

          <div class="col-md-6 text-center">
          <div class="card-testimonio">
                <img src="img/testimonio2.png">
                <p>"Muy buenos viajes, recomendado."</p>
                <ul>
                    <li><img src="img/star.png"></li>
                    <li><img src="img/star.png"></li>
                    <li><img src="img/star.png"></li>
                    <li><img src="img/star.png"></li>
                    <li><img src="img/star.png"></li>
                </ul>
                <h5>Juan Perez</h5>
            </div>
          </div>
      </div>
    </div>
    
  </div>

  <!--Las flechas del carousel-->
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
        </div>
    </div>

    <!--Formulario de contacto-->
    <div class="col-md-12 formulario">
        <p class="titulo">Contacto</p>
        <form action="index.php" class="form-contacto" role="form" method="post">
            <div class="row">
                <div class="col-md-12">
                    <input type="text" placeholder="Nombre y Apellido" name="nombre">
                </div>

                <div class="col-md-6">
                    <input type="text" placeholder="Dirección" name="direccion">
                </div>

                <div class="col-md-6">
                    <input type="text" placeholder="Telefono" name="telefono">
                </div>

                <div class="col-md-6">
                    <select name="" id="">
                        <option disabled selected>Seleccionar Ciudad</option>
                        <option value="Asunción">Asunción</option>
                        <option value="San Lorenzo">San Lorenzo</option>
                        <option value="Villa Elisa">Villa Elisa</option>
                    </select>
                </div>

                <div class="col-md-6">
                    <input type="text" placeholder="Codigo Postal" name="codigo">
                </div>

                <div class="col-md-12">
                    <input type="submit" name="submit" value="Enviar" class="btn-enviar">
                </div>
            </div>
        </form>
    </div>



<!--Pie de pagina-->
<?php
include('componentes/footer.html')
?>

<!--Archivos Javscript-->
<?php
include('componentes/js.html')
?>


<script>
//Funcion para ocultar y mostrar formulario avanzado en el banner
$(document).ready(function() {
    $('.formulario-avanzado').hide();

    $('.activar-formulario').click(function() {
        $('.formulario-banner').fadeOut();
        $('.formulario-avanzado').fadeIn();
    })
})
</script>

<script>
    AOS.init({
        disable: 'mobile'
    });
</script>

<!--Funcion del menu responsive-->
<script>
    $('.menu-responsive').hide();
    $('.lista-menu-responsive').hide();

    $('.boton-menu').click(function() {
        $('.lista-menu-responsive').fadeIn();
    })

</script>


</body>

</html>